# Career Progress Plan

1. Every 3 months update and reevaluate the career progress doc with your manager: https://docs.google.com/spreadsheets/d/1q-p-lSqGYyZy0RcCIuWHvYPk5aTiQfmOGGqY-7qYCHQ/edit#gid=984535776
1. Fork this project and make sure it's private. Invite your manager as a member.
1. Create [SMART goals](https://corporatefinanceinstitute.com/resources/knowledge/other/smart-goal/) based on the areas of improvement using the Goal template

[Career Matrix](https://about.gitlab.com/handbook/engineering/career-development/career-matrix.html)

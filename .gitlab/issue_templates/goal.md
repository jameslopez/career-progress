<!--

SMART goals are Specific, Measurable, Action-oriented, and Realistic with Timelines. When you set goals with these things in mind, you are likely to achieve the outcomes you want


Please set up your goal with a relevant title, description, and add comments to this issue with specific examples related to the goal.
-->

/label ~"smart goal"
